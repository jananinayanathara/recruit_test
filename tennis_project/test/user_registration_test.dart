import 'package:flutter_test/flutter_test.dart';
import 'package:tenizo/util/user_registration_util.dart';

void main() {
  test("Return the correct age", () {
    var today1 = new DateTime(2019, 05, 21);
    var today2 = new DateTime(2025, 05, 21);

    var dirthDay1 = new DateTime(2018, 05, 21);
    var dirthDay2 = new DateTime(1950, 05, 21);
    var dirthDay3 = new DateTime(1989, 05, 21);
    var dirthDay4 = new DateTime(2015, 05, 21);

    expect(UserRegistrationPageUtils.ageCalculation(today1, dirthDay1), "1");
    expect(UserRegistrationPageUtils.ageCalculation(today1, dirthDay2), "69");
    expect(UserRegistrationPageUtils.ageCalculation(today1, dirthDay3), "30");
    expect(UserRegistrationPageUtils.ageCalculation(today2, dirthDay2), "75");
    expect(UserRegistrationPageUtils.ageCalculation(today2, dirthDay4), "10");
  });

  test("Return the correct date format", () {
    var date1 = new DateTime(2015, 01, 21);
    var date2 = new DateTime(2015, 02, 21);
    var date3 = new DateTime(2015, 03, 21);
    var date4 = new DateTime(2015, 04, 21);
    var date5 = new DateTime(2015, 05, 21);
    var date6 = new DateTime(2015, 06, 21);
    var date7 = new DateTime(2015, 07, 21);
    var date8 = new DateTime(2015, 08, 21);
    var date9 = new DateTime(2015, 09, 21);
    var date10 = new DateTime(2015, 10, 21);
    var date11 = new DateTime(2015, 11, 21);
    var date12 = new DateTime(2015, 12, 21);

    expect(UserRegistrationPageUtils.dateFormatter(date1), "Jan 21 , 2015");
    expect(UserRegistrationPageUtils.dateFormatter(date2), "Feb 21 , 2015");
    expect(UserRegistrationPageUtils.dateFormatter(date3), "Mar 21 , 2015");
    expect(UserRegistrationPageUtils.dateFormatter(date4), "Apr 21 , 2015");
    expect(UserRegistrationPageUtils.dateFormatter(date5), "May 21 , 2015");
    expect(UserRegistrationPageUtils.dateFormatter(date6), "Jun 21 , 2015");
    expect(UserRegistrationPageUtils.dateFormatter(date7), "Jul 21 , 2015");
    expect(UserRegistrationPageUtils.dateFormatter(date8), "Aug 21 , 2015");
    expect(UserRegistrationPageUtils.dateFormatter(date9), "Sept 21 , 2015");
    expect(UserRegistrationPageUtils.dateFormatter(date10), "Oct 21 , 2015");
    expect(UserRegistrationPageUtils.dateFormatter(date11), "Nov 21 , 2015");
    expect(UserRegistrationPageUtils.dateFormatter(date12), "Dec 21 , 2015");
  });
}
