import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:url_launcher/url_launcher.dart' as URL;

class AppLauncher {
  AppLauncherListener _listener;
  AppLauncher(this._listener);

  runApp(subFunc, param) {
    switch (subFunc) {
      case ControllerSubFunc.launch_mailer:
        {
          openMailer(subFunc, param);
          break;
        }
      case ControllerSubFunc.launch_map:
        {
          openMap(subFunc, param);
          break;
        }
      default:
    }
  }

  openMailer(subFunc, param) async {
    var status = {};
    String url = "";
    if (param['to'] != null &&
        param['subject'] != null &&
        param['body'] != null) {
      url = "mailto:" +
          param['to'] +
          "?subject=" +
          param['subject'] +
          "&body=" +
          param['body'];
      if (param["cc"] != null) {
        url += "&cc=" + param["cc"];
      }
      if (param["bcc"] != null) {
        url += "&bcc=" + param["bcc"];
      }
    }

    try {
      if (await URL.canLaunch(url)) {
        await URL.launch(url, forceSafariVC: false, forceWebView: false);
        status['status'] = true;
        _listener.setAppLauncherStatus(subFunc, status);
      } else {
        status['status'] = false;
        _listener.setAppLauncherStatus(subFunc, status);
      }
    } catch (e) {
      status['status'] = false;
      _listener.setAppLauncherStatus(subFunc, status);
    }
  }

  openMap(subFunc, param) async {
    var status = {};
    String url = "";
    if (param['lat'] != null && param['long'] != null) {
      url = 'http://maps.google.com/maps?q=${param['lat']},${param['long']}';
    }

    try {
      if (await URL.canLaunch(url)) {
        await URL.launch(url, forceSafariVC: false, forceWebView: false);
        status['status'] = true;
        _listener.setAppLauncherStatus(subFunc, status);
      } else {
        status['status'] = false;
        _listener.setAppLauncherStatus(subFunc, status);
      }
    } catch (e) {
      status['status'] = false;
      _listener.setAppLauncherStatus(subFunc, status);
    }
  }
}

abstract class AppLauncherListener {
  setAppLauncherStatus(subFunc, param);
}
