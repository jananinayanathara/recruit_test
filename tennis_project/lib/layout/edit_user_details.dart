import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tenizo/tennizo_base_controller.dart';

class EditUser extends StatefulWidget {
  final arguments;
  final Function onSelected;

  @override
  _EditUser createState() {
    var editPlayerState = _EditUser();
    return editPlayerState;
  }

  EditUser({Key key, this.onSelected, this.arguments}) : super(key: key);
}

class _EditUser extends State<EditUser> with BaseControllerListner {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(54),
            child: AppBar(
              centerTitle: true,
              title: MediaQuery(
                  data: MediaQueryData(),
                  child: Text(
                    'Edit User Details',
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
                  )),
              leading: new IconButton(
                icon: new Icon(Icons.arrow_back_ios),
                onPressed: () => Navigator.of(context).pop(),
              ),
            )),
        body: Center(child:Text("TestPage"))
      ), onWillPop: () async => Future.value(false),
    );
  }

  @override
  resultFunction(func, subFunc, response) {
    return null;
  }
}
