import 'dart:ui';
import 'package:flutter_page_indicator/flutter_page_indicator.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

import '../util/app_const.dart';

class Help extends StatefulWidget {
  final arguments;
  final Function onSelected;

  @override
  _HelpState createState() => _HelpState();
  Help({Key key, this.onSelected, this.arguments}) : super(key: key);
}

class _HelpState extends State<Help> with BaseControllerListner {
  BaseController controller;

  @override
  void initState() {
    super.initState();
    controller = new BaseController(this);
  }

  static var deviceWidth = 0.0;
  static var deviceHeight = 0.0;
  var paddingData = const EdgeInsets.only(left: 210.0, top: 120.0);

  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      paddingData = const EdgeInsets.only(left: 190.0, top: 130.0);
    } else if (deviceWidth <= 400) {
      paddingData = const EdgeInsets.only(left: 200.0, top: 130.0);
    } else if (deviceWidth <= 450) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 500) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 550) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else {}
  }

  _pageNumber(int index) {
    if (index == 0) {
      return MediaQuery(
        data: MediaQueryData(),
        child: Container(
          color: AppColors.secondary_color,
          child: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('How do I Register a Player?',
                      style: TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 22.0)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 30, right: 40),
                    child: Container(
                        height: 330.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: new DecorationImage(
                            fit: BoxFit.contain,
                            image: new AssetImage(
                                "images/help_images/player_reg.png"),
                          ),
                        )),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 50, right: 40),
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                              'From here you can register players to create a players list. By creating player list easy to find the player name when you are going to start a game.',
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w500,
                                  height: 1.4)),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "1. You can select player's profile image using this camera button. \nTap the image icon and you need to allow the permission to get images from your gallery. \nThen select player image to display here.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "2. Here need to enter the player's name.It will display the player list to identify the player.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '3. Use this to select gender of the selected player',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '4. Use this to select date of birth  of the selected player.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '5. you can use this to select handedness of the selected player. From this select box need to select the type of the handedness of the player.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '6. From using this select box you need to select the type of the Style of the player.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '7. Select the team name of the selected player.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '8. Give the Role of player which is he/her representating in the match.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '9. After completed filling above fields, By tapping on the "ADD" button can register the player and you can see the registered player on your players list.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "If you want to Edit , Delete or View Registered Player's details , You can use the  button which is appear on the player list page,[Infront of the player name].",
                                style: TextStyle(
                                    fontSize: 19,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 50.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else if (index == 1) {
      return MediaQuery(
        data: MediaQueryData(),
        child: Container(
          color: AppColors.secondary_color,
          child: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('How do I Register a Stadium?',
                      style: TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 22.0)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 50, right: 40),
                    child: Container(
                        height: 340.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: new DecorationImage(
                            fit: BoxFit.contain,
                            image: new AssetImage(
                                "images/help_images/stadium_reg.png"),
                          ),
                        )),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 50, right: 40),
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                              "If you want to create stadium list, You can register details using this. By creating stadium list easy to find the stadium name when you are going to start a game.",
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w500,
                                  height: 1.4)),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                               "1. You can select Stadium's profile image using this camera button. \nTap the image icon and you need to allow the permission to get images from your gallery. \nThen select stadium image to display here.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "2. Use this field to register Stadium name ,It will display on the stadium list to identify the stadium.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "3. Use this can add address where stadium located.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text("4. Add contact number of the stadium.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "5. Select the number of courts under this selected stadium.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text('6. Add stadium reservation URL, You can easily find the reservation details.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '7. Add GPS location where stadium located.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "8. After completed filling above fields, By tapping on the \"Register\" button can register a Stadium and you can see the registered stadium on your Stadium list.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "If you want to Edit , Delete or View Registered Stadium details , You can use the button which is appear on the stadium list page,[Infront of the stadium name]",
                                style: TextStyle(
                                    fontSize: 19,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else if (index == 2) {
      return MediaQuery(
        data: MediaQueryData(),
        child: Container(
          color: AppColors.secondary_color,
          child: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('How do I Register a Court?',
                      style: TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 22.0)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 60, right: 40),
                    child: Container(
                        height: 300,
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: new DecorationImage(
                            fit: BoxFit.contain,
                            image: new AssetImage(
                                "images/help_images/court_reg.png"),
                          ),
                        )),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 50, right: 40),
                    child: Container(
                      // width: 320,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                              "If you want to create court list, First you need to register a Stadium,Then You can create court list which is under registered stadium using the '+' button under the selected stadium name.",
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w500,
                                  height: 1.4)),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "1. You can select Court's profile image using this camera button. \nTap the image icon and you need to allow the permission to get images from your gallery. \nThen select court image to display here.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "2. Use this field to register Court name or Court ID.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text("3. Select the selected Court Surface type.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "4. Select Court location type (Indoor/ Outdoor).",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '5. Add court available time, To easily find the available times of the selected court',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '6. After completed filling above fields, By tapping on the \"Register\" button can register a Court and you can see the registered courts on your court list under selected Stadium name on the stadium list page.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "If you want to Edit , Delete or View Registered Court details , You can use the button which is appear on the court list[Infront of the court name]",
                                style: TextStyle(
                                    fontSize: 19,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else if (index == 3) {
      return MediaQuery(
        data: MediaQueryData(),
        child: Container(
          color: AppColors.secondary_color,
          child: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('How do I Add a New Game?',
                      style: TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 22.0)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 50, right: 40),
                    child: Container(
                        height: 400.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: new DecorationImage(
                            fit: BoxFit.contain,
                            image: new AssetImage(
                                "images/help_images/game_setting.png"),
                          ),
                        )),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 50, right: 40),
                    child: Container(
                      // width: 320,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                              "If you need to start the new game you need to enter the game details here.",
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w500,
                                  height: 1.3)),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "1. Here need to enter match name and match round.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text("2. Name of the player A.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text("3. Name of the Player B.",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "4. Number of games which have selected match.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '5. Number of sets of the selected match.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '6. Select whether in started time of the match.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "7. Select the temperature in started time of the match in to \"Temperature in Celsius\" fields.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '8. Select the type of Advantage , Semi advantage or not.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '9. Select the type of the Tie break.You can select tie break, Super Tie break or not.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "10. Select the Stadium that selected match is going to be started.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                ' 11. Select the name of the court / court number which is going to be start the match , under above selected stadium .',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '12. Select the starting date of the game.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "13. Select the starting time of the game.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '14. After completed the above fields tap the "New Game" button and you can create new game to record the scores when started the match.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: Text(
                                "Then you will redirect to the score page.",
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w500,
                                    height: 1.3)),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else if (index == 4) {
      return MediaQuery(
        data: MediaQueryData(),
        child: Container(
          color: AppColors.secondary_color,
          child: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('How do I Add Scores of the match?',
                      style: TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 22.0)),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, right: 40),
                    child: Container(
                        height: 380.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: new DecorationImage(
                            fit: BoxFit.contain,
                            image: new AssetImage(
                                "images/help_images/score_board.png"),
                          ),
                        )),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 50, right: 40),
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                              'First you can see Score board of the playe A and Player B :',
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w500,
                                  height: 1.4)),
                          Padding(
                            padding: const EdgeInsets.only(top: 5, left: 20),
                            child: Text('1. Score board of the player A.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Text('2. Score board of the player B.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '3. From here you can see the \"Stat\" of the current match.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '4. From here you can see the \"Flow page\" of the current match.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                "5. If you want to update the details of the current match you can redirect to the game setting page and can updtae details.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '6. If you need to undo the added score of the match, Using this you can undo the last added score.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '7. If you want you change the rally count here. You can increase or decrease the rally count.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '8. Using this button you can pause the game here and redirect to the game history page (Home Page).',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '9. From using these buttons you can add score of the players according to their performance.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else if (index == 5) {
      return MediaQuery(
        data: MediaQueryData(),
        child: Container(
          color: AppColors.secondary_color,
          child: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('How do I view Game History?',
                      style: TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 22.0)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 24, right: 40),
                    child: Container(
                        height: 380.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: new DecorationImage(
                            fit: BoxFit.contain,
                            image: new AssetImage(
                                "images/help_images/game_history.png"),
                          ),
                        )),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 50, right: 40),
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                              'If you want to see the game history when you have started a new game , First you need to end the game or pause the game.After that automatically redirect to the game history page.',
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w500,
                                  height: 1.4)),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '1. This is the Game history of single match. If you have end or pause multiple games, here you can see list of that',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text('Then, If you tap following buttons;',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '2. Tap on the \"END\" button will redirect to the \"Stat page\" and you can see the Stats of that match.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Text(
                                '3. Tap on the \"Pause\" button will redirect to the \"Score board\" and you can start adding scores for the paused match again.',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1.4)),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 60.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else if (index == 6) {
      return MediaQuery(
        data: MediaQueryData(),
        child: Container(
          color: AppColors.secondary_color,
          child: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('What is Stat of the Game?',
                      style: TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 22.0)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, right: 40),
                    child: Container(
                        height: 400,
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: new DecorationImage(
                            fit: BoxFit.contain,
                            image:
                                new AssetImage("images/help_images/stat.png"),
                          ),
                        )),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 50, right: 40),
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('・Here you can see the Stat of the two players.',
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w500,
                                  height: 1.4)),
                          Text(
                              '・You can see the stat while entering scores of the match using score board and after ending the match.',
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w500,
                                  height: 1.4)),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else if (index == 7) {
      return MediaQuery(
        data: MediaQueryData(),
        child: Container(
          color: AppColors.secondary_color,
          child: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('What is Flow of the Game?',
                      style: TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 22.0)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 75, right: 100),
                    child: Container(
                      height: 300,
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        //border: Border.all(width: 1),
                        image: new DecorationImage(
                          fit: BoxFit.contain,
                          image: new AssetImage("images/help_images/flow.png"),
                          
                        ),
                      )
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 50, right: 40),
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                              "・Here you can see the game Flow of the two player's scores after ending the game.",
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w500,
                                  height: 1.4)),
                                  Text(
                              '・You can see the flow while entering scores of the match using score board and after ending the match.',
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w500,
                                  height: 1.4)),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();

    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: MediaQuery(data: MediaQueryData(),
           child:Text('Help Center',
           style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700))),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: MediaQuery(
            data: MediaQueryData(),
            child: new Swiper(
              itemBuilder: (BuildContext context, int index) {
                return _pageNumber(index);
              },
              indicatorLayout: PageIndicatorLayout.COLOR,
              // autoplay: false,
              itemCount: 8,
              pagination: new SwiperPagination(
                  builder: DotSwiperPaginationBuilder(
                activeColor: AppColors.primary_color,
                color: Color(0xFFC2F0F0),
              )),
              control:
                  new SwiperControl(color: AppStyle.primary_color, size: 40),
            )),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  @override
  resultFunction(func, subFunc, response) {}
}
