import 'dart:ui';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../util/app_const.dart';

class HelpPage extends StatefulWidget {
  final arguments;
  final Function onSelected;

  @override
  _HelpPageState createState() => _HelpPageState();
  HelpPage({Key key, this.onSelected, this.arguments}) : super(key: key);
}

class _HelpPageState extends State<HelpPage> with BaseControllerListner {
  BaseController controller;

  @override
  void initState() {
    super.initState();
    controller = new BaseController(this);
  }

  static var deviceWidth = 0.0;
  static var deviceHeight = 0.0;
  var paddingData = const EdgeInsets.only(left: 210.0, top: 120.0);

  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      paddingData = const EdgeInsets.only(left: 190.0, top: 130.0);
    } else if (deviceWidth <= 400) {
      paddingData = const EdgeInsets.only(left: 200.0, top: 130.0);
    } else if (deviceWidth <= 450) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 500) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 550) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else {}
  }

  _directToTermsConditions() {
    Navigator.of(context).pushNamed('/TermsConditionsPage');
  }
  _directToPrivacyPolicy() {
    Navigator.of(context).pushNamed('/PrivacyPolicy');
  }
  _directToHelp() {
    Navigator.of(context).pushNamed('/Help');
  }

  _directToLicensePage(
      {@required BuildContext context,
      String applicationName,
      String applicationVersion,
      Widget applicationIcon,
      String applicationLegalese}) {
    assert(context != null);
    Navigator.push(
        context,
        MaterialPageRoute<void>(
            builder: (BuildContext context) => LicensePage(
                applicationName: applicationName,
                applicationVersion: applicationVersion,
                applicationLegalese: applicationLegalese)));
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();

    return WillPopScope(
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:
                MediaQuery(data: MediaQueryData(), child: Text('Help',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700))),
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ),
        body: MediaQuery(
          data: MediaQueryData(),
          child: Container(
            child: SingleChildScrollView(
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 30.0,
                    ),
                    FlatButton(
                      onPressed:_directToTermsConditions,
                      padding: const EdgeInsets.only(left: 15),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: 50.0,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Terms and Condtions",
                                style: TextStyle(fontSize: 18),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        _directToLicensePage(
                            context: context,
                            applicationName: 'TenniZo',
                            applicationVersion: '');
                      },
                      padding: const EdgeInsets.only(left: 15),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: 50.0,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Libraries and Frameworks",
                                style: TextStyle(fontSize: 18),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    FlatButton(
                      onPressed: _directToPrivacyPolicy,
                      padding: const EdgeInsets.only(left: 15),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: 50.0,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Privacy Policy",
                                style: TextStyle(fontSize: 18),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                     FlatButton(
                      onPressed: _directToHelp,
                      padding: const EdgeInsets.only(left: 15),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: 50.0,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Help Center",
                                style: TextStyle(fontSize: 18),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  @override
  resultFunction(func, subFunc, response) {}
}
