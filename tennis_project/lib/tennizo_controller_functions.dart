enum ControllerFunc {
  db_sqlite,

  launch_app
}

enum ControllerSubFunc {
  db_insert,
  db_select,
  db_update,
  db_delete,
  db_select_batch,

  launch_mailer,
  launch_map,
}
